# Network Plugin Tolerations

According to <https://github.com/hetznercloud/hcloud-cloud-controller-manager>, depending on your network plugin you might need to patch the tolerations to get up the networking plugin running before the nodes are initialized.

## flannel

```shell
kubectl -n kube-system patch ds kube-flannel-ds --type json -p '[{"op":"add","path":"/spec/template/spec/tolerations/-","value":{"key":"node.cloudprovider.kubernetes.io/uninitialized","value":"true","effect":"NoSchedule"}}]'
```
